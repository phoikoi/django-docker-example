# django-docker-example

I created this little project to serve as an example of one way to dockerize
a Django project.  I make no claim for this being the only way, or the best way.

The dockerfiles are based on another image that is published to the Docker Hub.  I
have included the dockerfile for that base image here as `base-Dockerfile`, for your
reference.  The base image takes a while to build, especially on an ARM device like
a Raspberry Pi, so a derivative image like this one saves some build time.

The `build.sh` script builds an image whose name includes the (short) git commit hash,
and its tag is the platform it was built on.  Of course, one can always re-tag the image
with whatever name is desired, to push it to a specific registry, etc.

Running the image with a command of `dev` will run `pip` to upgrade the python packages
per the `requirements.txt` file.  I like to use the `pip-compile` package to manage the
requirements, which takes its input from `requirements.in` and generates the
`requirements.txt` file.  That way, one only has to specify the top-level packages necessary
(for human convenience in reading), and the code is happy with its very specific list
of packages to work with.

The `dev` command will also run the Django `migrate` and `collectstatic` management commands,
and then start up the `runserver` command for development.  The default command, `start`, does
not run any management commands or upgrade packages.  There is also a `migrate` command that
only performs that task, as it may be necessary to run that on a production database by itself.

I assume that you will add the extra Dockerfile statements necessary to serve your needs, including perhaps a data volume shared with your reverse proxy (such as `nginx`) to serve
your static files, unless you use a CDN or other such solution for those.

**NOTE:** you can build the `armhf` image on an amd64 host, if you use the *binfmt-misc*
technique described [here](https://eyskens.me/multiarch-docker-images/); in short, enter
`docker run --rm --privileged multiarch/qemu-user-static:register --reset` and you should
be able to run and build ARM images on your Intel system.  Sadly, the reverse is not true;
the tools have not been yet built to allow ARM to build Intel images.  But, it would probably
be far too slow anyway...

I would like to also thank [Jakub Skałecki](http://linkedin.com/in/jskalec) for his
excellent article on
[how to write excellent dockerfiles](https://rock-it.pl/how-to-write-excellent-dockerfiles/).
You may notice some artifacts of his examples lurking here in my own code.

Enjoy!

