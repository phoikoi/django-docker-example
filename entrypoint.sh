#!/usr/bin/env sh
CMD=$1

case "$CMD" in
    "dev" )
        git pull
        pip install --upgrade -r requirements.txt
        python manage.py migrate
        python manage.py collectstatic --noinput
        exec python manage.py runserver
        ;;

    "migrate" )
        exec python manage.py migrate
        ;;

    "collectstatic" )
        exec python manage.py collectstatic --noinput
        ;;

    "start" )
        source /app/gunicorn.sh
        ;;

    * )
        exec $CMD ${@:2}
        ;;

esac

        
