#!/usr/bin/env bash
ARCH=$(uname -m)
IMAGE=django-docker-example
VERSION="$(git rev-parse --short HEAD)"
case "$ARCH" in
    "x86_64" )
        ARCH_NAME=amd64
        ;;
    "armv7l" )
        ARCH_NAME=armhf
        ;;
    * )
        echo Unknown architecture ${ARCH}!
        exit 1
    ;;
esac

IMAGE_NAME=${IMAGE}-${VERSION}:${ARCH_NAME}
docker build -f Dockerfile-${ARCH_NAME} -t ${IMAGE_NAME} .
docker tag ${IMAGE_NAME} dde-latest

